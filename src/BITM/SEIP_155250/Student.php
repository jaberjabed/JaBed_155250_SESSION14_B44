<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 1/18/2017
 * Time: 9:41 AM
 */

namespace Tap;


use App\Person;

class Student extends Person
{
    private $studentID;

    public function setStudentID($studentID)
    {
        $this->studentID = $studentID;
    }

    public function getStudentID()
    {
        return $this->studentID;
    }
}