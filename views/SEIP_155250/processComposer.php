<?php

require_once("../../vendor/autoload.php");



if(empty($_POST['studentID'])){

    echo "I'm a Person! <br>";

    $objPerson = new \app\Person();

    $objPerson->setName($_POST['userName']);
    $objPerson->setDateOfBirth($_POST['dateOfBirth']);

    echo $objPerson->getName() . "<br>";
    echo $objPerson->getDateOfBirth() . "<br>";
}
else{

    echo "I'm a Student! <br>";

    $objStudent = new \Tap\Student();

    $objStudent->setName($_POST['userName']);
    $objStudent->setStudentID($_POST['studentID']);
    $objStudent->setDateOfBirth($_POST['dateOfBirth']);

    echo $objStudent->getName() . "<br>";
    echo $objStudent->getStudentID() . "<br>";
    echo $objStudent->getDateOfBirth() . "<br>";
}